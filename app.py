#!/bin/env python3

import os
import sys

from termcolor import colored

class Test:
	def __init__(self, mouliBin, studentBin):
		self.__mouli = mouliBin
		self.__student = studentBin
		self.__tests = range(30)
		self.__error_mode = False
		self.logs = []
	
	def run(self):
		for i in self.__tests:
			self.logs.append({
				"size": i,
				"status": self.__launchTests(i)
			})

	def printLogs(self):
		for test in self.logs:
			print("Arg {}: ".format(test["size"]), colored(test["status"], 'red') if test["status"] == "KO" else colored(test["status"], 'green'))

	def __launchTests(self, nb):
		mouli = os.popen(self.__mouli + " " + str(nb)).read()
		student = os.popen(self.__student + " " + str(nb)).read()
		return ("OK" if student == mouli else "KO")
		
		

def main(m, s):
	test = Test(m, s)
	test.run()
	test.printLogs()

def help():
	print("python3 app.py [bin] [bin]")

if __name__ == '__main__':
	if len(sys.argv) != 3:
		exit(1)
	if os.path.isfile(sys.argv[1]) and os.path.isfile(sys.argv[2]):
		main(sys.argv[1], sys.argv[2])
	else:
		print("Can't find binaries or format like this: ./bin")